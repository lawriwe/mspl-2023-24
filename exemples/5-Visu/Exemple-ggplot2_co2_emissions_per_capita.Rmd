---
title: "Simple plot examples (of ggplot2) using CO2 emissions historical data"
author: "Danilo Carastan dos Santos"
date: "February 05, 2024"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

This is a demonstration of how ggplot works. Using Our World in Data's historical data on CO2 emissions per capita.

We first start by reading the input data

```{r}
library(tidyverse)
library(ggplot2)
CO2Data <- read_delim("./data/owid-co2-data.csv",delim=",");

CO2Data %>% head(10)
```

Now we collect data regarding only to France, and only to World (the sum of all 
countries we have data).

I also computed a gdp growth per year (gdp_diff) and a population growth per year
(population_diff)

```{r}
CO2DataFrance <- CO2Data %>%
filter(country == "France") %>%
mutate(gdp_diff = gdp - lag(gdp)) %>%
mutate(population_diff = population - lag(population))

CO2DataWorld <- CO2Data %>%
filter(country == "World") %>%
mutate(gdp_diff = gdp - lag(gdp)) %>%
mutate(population_diff = population - lag(population))

CO2DataFrance %>% head(10)
```

This first example is a one-dimensional bar plot of the emissions data.
In this case what the height of the bar mean?

```{r}
CO2DataFrance %>%
  ggplot() + 
  aes(x = country, y = co2) + # Replace 'year' with your actual x-axis variable
  geom_bar(stat = "identity", width = 0.5) +
  theme_minimal(base_size = 22) +
  labs(
    title = "Annual CO2 emissions\nfrom fossil fuels in France",
    x = "France",
    y = "CO2 emissions (million tonnes)"
  ) +
  theme(plot.title = element_text(size = rel(1)), 
        axis.title.x = element_text(size = rel(1)), 
        axis.title.y = element_text(size = rel(1))) 
```


Below now is an example of a box plot of the CO2 emissions data, treated as a
one-dimensional data (i.e., just a distribution of numbers).

The horizontal line is the median. The upper and lower are the 75 (Q3) and 25 (Q1) 
percentiles, respectively. (the box height is therefore the interquartile range,
IQR). The vertical top and bottom lines are the "maximum" (Q3 + 1.5 * IQR), and
"minimum" (Q1 - 1.5 * IQR). Values outside of this range of minimum and maximum
are displayed as outliers (vertical dots, not present in the example below)


```{r}
CO2DataFrance %>%
  ggplot() + 
  aes(x = country, y = co2) + # Replace 'year' with your actual x-axis variable
  geom_boxplot(width = 0.5) +
  theme(text = element_text(size = 22)) + # Setting font size globally
  theme_minimal(base_size = 22) +
  labs(
    title = "Annual CO2 emissions\nfrom fossil fuels in France",
    x = "France", 
    y = "CO2 emissions (million tonnes)"
  ) +
  theme(plot.title = element_text(size = rel(1)), 
        axis.title.x = element_text(size = rel(1)), 
        axis.title.y = element_text(size = rel(1))) 
```

Now we are on two dimensions. We consider the CO2 emissions in function of each
year. In the example below we are just using a lineplot to visualize the data.

```{r}
CO2DataFrance %>%
  ggplot() + 
  aes(x = year, y = co2) + # Replace 'year' with your actual x-axis variable
  geom_line(width = 0.5) +
  theme(text = element_text(size = 22)) + # Setting font size globally
  theme_minimal(base_size = 22) +
  labs(
    title = "Annual CO2 emissions\nfrom fossil fuels in France",
    x = "France", 
    y = "CO2 emissions (million tonnes)"
  ) +
  theme(plot.title = element_text(size = rel(1)), 
        axis.title.x = element_text(size = rel(1)), 
        axis.title.y = element_text(size = rel(1))) 
```
Now we jump to three dimensions. Here we do a scatter plot (e change the lines by)
geometric objects (circles, triangles, etc). We use the fact that we can change the color
of the objects to show more data.

Keep in mind that colors (color palettes for categorical data or color maps for
numerical data) must be color blind friendly (see reference below)

https://www.nceas.ucsb.edu/sites/default/files/2022-06/Colorblind%20Safe%20Color%20Schemes.pdf

```{r}
p <- ggplot(data = CO2DataFrance, aes(x = year, y = co2, color = gdp_diff)) +
  geom_point() + # Creates a scatter plot
  scale_color_viridis_c() + # Uses the viridis color scale
  labs(
    y = "CO2 emissions\n(million tonnes)",
    x = "Year",
    color = "GDP Growth ($)"
  ) +
  theme_minimal(base_size = 22) +
  theme(text = element_text(size = 22), # Global font size
        plot.title = element_text(size = rel(1.5)),
        axis.title.x = element_text(size = rel(1.2)),
        axis.title.y = element_text(size = rel(1.2)),
        legend.title = element_text(size = rel(0.8)),
        legend.text = element_text(size = rel(1))) 

# Format the legend labels
p <- p + guides(color = guide_colourbar(
  title = "GDP Growth ($)",
  label.position = "left",
  barwidth = 2,
  barheight = 10,
  label.theme = element_text(size = 10)
))

p
```


The graph below is the same but i changed the color map. The trick was to quickly
identify years where the GDP increased (red) or decreased (blue). It "works" but not quite,
since the colorbar is not really centered at zero (i.e., the "white" part does not align with
zero). How to fix this?

There is also another problem regarding multiple ways we can print this graph in a paper.
Can you figure out that problem this is?

```{r}
p <- ggplot(data = CO2DataFrance, aes(x = year, y = co2, color = gdp_diff)) +
  geom_point() + # Creates a scatter plot
  scale_color_gradientn(colors = rev(RColorBrewer::brewer.pal(11, "RdBu"))) + # Using a color scale similar to "coolwarm"
  labs(
    y = "CO2 emissions\n(million tonnes)",
    x = "Year",
    color = "GDP Growth ($)"
  ) +
  theme_minimal(base_size = 22) +
  theme(text = element_text(size = 22), # Global font size
        plot.title = element_text(size = rel(1.5)),
        axis.title.x = element_text(size = rel(1.2)),
        axis.title.y = element_text(size = rel(1.2)),
        legend.title = element_text(size = rel(0.8)),
        legend.text = element_text(size = rel(1))) 

# Format the legend labels
p <- p + guides(color = guide_colourbar(
  title = "GDP Growth ($)",
  label.position = "left",
  barwidth = 2,
  barheight = 10,
  label.theme = element_text(size = 10)
))

p
```

The plot below i added a new dimension, the country. I'm showing CO2 emissions data
from France and from the World (the sum of all countries that we have data).
I added this dimension by changing the shape of the objects in the scatter plot.
A circle represents data from France, and a triangle represents data from the World.

We can see that emissions increase exponentially for the World, but we can't see
too much for France. What is going on? can we improve this graph?


```{r}
CO2WorldFrance <- CO2Data %>% 
  filter(country == "France" | country == "World") %>% 
  mutate(gdp_diff = gdp - lag(gdp)) %>%
  mutate(population_diff = population - lag(population))

# Create the scatter plot
p <- ggplot(CO2WorldFrance, aes(x = year, y = co2, color = population_diff, shape = country)) +
  geom_point(size = 1.5) +  # Adjust the size of the points
  scale_color_viridis_c(option = "plasma") +  # Use viridis color scale, plasma option
  scale_shape_manual(values = c(16, 17)) +  # Manual shapes for 'country'
  labs(
    x = "Year",
    y = "CO2 emissions\n(million tonnes)",
    color = "Population\nGrowth",  # New line for legend title
    shape = "Country"
  ) +
  theme_minimal(base_size = 22) +
  theme(
    legend.title = element_text(size = 18),
    legend.text = element_text(size = 16),
    legend.position = "right"
  ) +
  guides(
    color = guide_colourbar(title.position = "top", title.hjust = 0.5, label.hjust = .5),
    shape = guide_legend(override.aes = list(size = 6))
  )

p
```

I improved the above graph by changing the scale on the Y axis, from linear to a
logarithmic scale. Now we can see both curves, France and World, better.

Using logarithmic scale can be dangerous. For example, look for the World curve 
in the graph above (linear scale) and in the graph below (log scale).

In short, a straight line in a graph in log scale in the Y axis represents an exponential
curve!

When using log scale, you must explicit this information in the graph.

```{r}
# Create the scatter plot
p <- ggplot(CO2WorldFrance, aes(x = year, y = co2, color = population_diff, shape = country)) +
  geom_point(size = 1.5) +  # Adjust the size of the points
  scale_color_viridis_c(option = "plasma") +  # Use viridis color scale, plasma option
  scale_shape_manual(values = c(16, 17)) +  # Manual shapes for 'country'
  scale_y_log10() + # Log10 scale on the y axis
  labs(
    x = "Year",
    y = "CO2 emissions\n(million tonnes)\nlog scale",
    color = "Population\nGrowth",  # New line for legend title
    shape = "Country"
  ) +
  theme_minimal(base_size = 22) +
  theme(
    legend.title = element_text(size = 18),
    legend.text = element_text(size = 16),
    legend.position = "right"
  ) +
  guides(
    color = guide_colourbar(title.position = "top", title.hjust = 0.5, label.hjust = .5),
    shape = guide_legend(override.aes = list(size = 6))
  )

p
```

The graph below shows the same information of the graph above, but i plotted the
two curves in two separate subgraphs (instead of superposing them into a single 
graph).

```{r, fig.height = 5, fig.width = 20}
library(ggplot2)
library(gridExtra) # For arranging plots side by side
library(scales)    # For formatting

# Plot for France
p_france <- ggplot(CO2DataFrance, aes(x = year, y = co2, color = population_diff)) +
  geom_point(size = 4) +
  scale_color_viridis_c(limits = c(min(CO2DataFrance$population_diff, na.rm = TRUE), max(CO2DataFrance$population_diff, na.rm = TRUE))) +
  scale_y_continuous(trans="log10", limits=c(1, 4e4)) +
  labs(title = "France",
       x = "Year",
       y = "CO2 emissions (million tonnes)\nlog scale",
       color = "Population Growth") +
  theme_minimal(base_size = 22)

# Plot for World
p_world <- ggplot(CO2DataWorld, aes(x = year, y = co2, color = population_diff)) +
  geom_point(size = 4) +
  scale_color_viridis_c(limits = c(min(CO2DataWorld$population_diff, na.rm = TRUE), max(CO2DataWorld$population_diff, na.rm = TRUE))) +
  scale_y_continuous(trans="log10", limits=c(1, 4e4)) + 
  labs(title = "World",
       x = "Year",
       y = "CO2 emissions (million tonnes)\nlog scale",
       color = "Population Growth") +
  theme_minimal(base_size = 22)

# Arrange the plots side by side
grid.arrange(p_france, p_world, ncol = 2)

```
